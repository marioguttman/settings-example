﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//ADD configuration
using System.Configuration;

namespace SettingsExample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Configuration config = null;

        public MainWindow()
        {
            InitializeComponent();

            //retieve settings values
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(this.GetType().Assembly.Location);
                string username = "";

                if (config != null)
                    username = GetAppSetting("username");
                else
                {
                    MessageBox.Show("Problem getting the configuration file!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                //write username value to the textbox
                usernameField.Text = username;

            }
            catch (Exception ex1)
            {
                MessageBox.Show("exception: " + ex1);
            }


        }

        private void saveBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(this.GetType().Assembly.Location);
                config.AppSettings.Settings["username"].Value = usernameField.Text;
                config.Save(ConfigurationSaveMode.Modified);
            }
            catch (Exception ex1)
            {
                MessageBox.Show("exception: " + ex1);
            }
        }
        public string GetAppSetting(string key)
        {
            KeyValueConfigurationElement element = config.AppSettings.Settings[key];
            if (element != null)
            {
                string value = element.Value;
                if (!string.IsNullOrEmpty(value))
                    return value;
            }
            else
            {
                config.AppSettings.Settings.Add(key, "");
                config.Save(ConfigurationSaveMode.Modified);
            }
            return string.Empty;
        }
    }
}
